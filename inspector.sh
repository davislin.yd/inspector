#!/bin/bash

echo -e "Function list:
\t1: Check the wget
\t2: Check the mtr
\t3: Check the pyenv
\t4: Check the python3"

echo -n "Please choose one option: "
read choice

if [ ${choice} -eq '1' ]; then
    echo "You choose wget"

    which wget > /dev/null 2>&1
    if [ ! $? -eq '0' ]; then
        echo -e ">> Could not found wget, now install it..."
        yum install -y wget > /dev/null && echo -e 'wget installed\n'
    else
        echo -e "wget location: $(which wget)\n"
    fi

elif [ ${choice} -eq '2' ]; then
    echo "You choose mtr"

    which mtr > /dev/null 2>&1

    if [ $? -eq 0 ]; then
        echo -e ">> The command mtr exist."
    else
        echo -e ">> Could not found mtr..."
        echo -e ">> Now installing mtr from yum repo"
        #yum install -y mtr > /dev/null 2>&1 && ls -l $(which mtr)

        if [ ! $(cat /etc/redhat-release | awk -F' ' '{print $1}') -eq 'CentOS' ]; then
            exit
        fi

        yum install -y wget gcc autoconf

        cd /root

        wget ftp://ftp.bitwizard.nl/mtr/mtr-0.92.tar.gz -O ./mtr-0.92.tar.gz

        tar zxvf ./mtr-0.92.tar.gz && cd ./mtr-0.92
        ./configure && make && make install
    fi
elif [ ${choice} -eq '3' ]; then

    which pyenv > /dev/null 2>&1

    if [ $? -eq 0 ]; then
        echo -e ">> The command pyenv exist."
    else
        yum install -y sqlite-devel
        yum install -y gcc zlib-devel.x86_64 bzip2.x86_64 bzip2-devel.x86_64 readline-devel.x86_64 openssl-devel.x86_64

        curl -L https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer | bash

        echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bash_profile
        echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bash_profile
        echo 'eval "$(pyenv init -)"' >> ~/.bash_profile
        echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bash_profile

        source ~/.bash_profile

        pyenv -v

        # pyenv install 2.7.14
        # pyenv install 3.6.2
    fi
elif [ ${choice} -eq '4' ]; then
    bash ./install-python3.sh
fi
